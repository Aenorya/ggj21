﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public static Controls controls;

    private void Awake()
    {
        if (controls != null)
        {
            Destroy(this);
        }
        else
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            controls = new Controls();
            controls.Enable();
        }
    }
}
