﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager instance;
    public int scorePoints = 75;

    public int winPoints = 10;
    public int losePoints = 20;
    public int maxTimeBonus = 10;

    public int scoreObjective = 150;
    
    public GameEvent scoreUpdate;
    public GameEvent onLoss;
    public GameEvent onWin;
    
    private void Awake()
    {
        if (instance)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    public void GainPoints(float remainingTimeFactor = 0)
    {
        scorePoints = Mathf.Clamp(scorePoints + winPoints + Mathf.RoundToInt(maxTimeBonus * remainingTimeFactor), 0, scoreObjective);
        
        if (scorePoints >= scoreObjective)
        {
            Win();
        }
        scoreUpdate.Notify();
    }

    public void LosePoints()
    {
        scorePoints = Mathf.Clamp(scorePoints - losePoints, 0, scoreObjective);
        if (scorePoints <= 0)
        {
            Lose();
        }
        scoreUpdate.Notify();
        
    }

    public void Lose()
    {
        onLoss.Notify();
    }

    public void Win()
    {
        onWin.Notify();
    }
}
