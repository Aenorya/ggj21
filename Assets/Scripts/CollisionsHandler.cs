﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionsHandler : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("LnF"))
        {
            GetComponentInChildren<ObjectsPicker>().canGiveDropBox = true;
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("LnF"))
        {
            GetComponentInChildren<ObjectsPicker>().canGiveDropBox = false;
        }
    }
}
