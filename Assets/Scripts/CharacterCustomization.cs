﻿using UnityEngine;
using System.Collections.Generic;

public class CharacterCustomization : MonoBehaviour
{
    [SerializeField]
    float HasHatProbabilty = 0.25f;
    bool HasHat = false;

    [SerializeField]
    private List<GameObject> Hats = new List<GameObject>();

    [SerializeField]
    private List<GameObject> Hairs = new List<GameObject>();

    [SerializeField]
    private List<GameObject> TShirts = new List<GameObject>();

    [SerializeField]
    private List<GameObject> Suits = new List<GameObject>();

    [SerializeField]
    private List<GameObject> Legs = new List<GameObject>();

    [SerializeField]
    private List<GameObject> Feets = new List<GameObject>();

    private void InitHasHat()
    {
        HasHat = Random.Range(0.0f, 1.0f) <= HasHatProbabilty;
    }
    private int GetRandomIndex(List<GameObject> _list)
    {
        return Random.Range(0, _list.Count);
    }

    private void Start()
    {
        InitHasHat();

        int hatIndex = GetRandomIndex(Hats);
        for (int i = 0; i < Hats.Count; ++i)
        {
            Hats[i].SetActive(HasHat && i == hatIndex);
        }

        int hairIndex = GetRandomIndex(Hairs);
        for (int i = 0; i < Hairs.Count; ++i)
        {
            Hairs[i].SetActive(i == hairIndex);
        }

        int tshirtIndex = GetRandomIndex(TShirts);
        for (int i = 0; i < TShirts.Count; ++i)
        {
            TShirts[i].SetActive(i == tshirtIndex);
        }

        int suitIndex = GetRandomIndex(Suits);
        for (int i = 0; i < Suits.Count; ++i)
        {
            Suits[i].SetActive(i == suitIndex);
        }

        int legIndex = GetRandomIndex(Legs);
        for (int i = 0; i < Legs.Count; ++i)
        {
            Legs[i].SetActive(i == legIndex);
        }

        int feetIndex = GetRandomIndex(Feets);
        for (int i = 0; i < Feets.Count; ++i)
        {
            Feets[i].SetActive(i == feetIndex);
        }
    }
}
