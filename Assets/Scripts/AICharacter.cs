﻿using UnityEngine;
using UnityEngine.AI;

public class AICharacter : MonoBehaviour
{
    public enum State
    {
        None,
        MoveToAssignedTable,
        SittingDown,
        Eat,
        GettingUp,
        FindSearchPos,
        MoveToSearchPos,
        Search,
        Happy,
        Angry,
        LeaveScene,
    }

    public State m_state = State.None;
    [SerializeField]
    public State CurrentState
    {
        get { return m_state; }
        set { SetState(value); }
    }

    NavMeshAgent Agent = null;
    Animator AnimatorComponent;

    AIPlanningManager.CharacterHandler AssignedCharacter = null;
    AIPlanningManager.TableHandler AssignedTable = null;
    [SerializeField]
    AIPlanningManager.LostObjectHandler AssignedLostObject = null;
    AIPlanningManager.SearchPositionHandler AssignedSearchPos = null;

    [SerializeField]
    bool HasFoundLostObject = false;

    [SerializeField]
    float EatDuration = 5.0f;

    [SerializeField]
    float HappyDuration = 4.0f;
    
    [SerializeField]
    float DestinationEpsilon = 1.0f;

    [SerializeField]
    float SearchDuration = 30.0f;

    [SerializeField]
    float AngryDuration = 10.0f;

    [SerializeField]
    bool IsSad = false;
    bool GiveObjectStarted = false;

    [SerializeField]
    GameObject CocaCup = null;

    [Header("Debug (read only)")]
    [SerializeField]
    float TimeEatElapsed = 0.0f;

    [SerializeField]
    float TimeHappyElapsed = 0.0f;

    [SerializeField]
    float TimeSearchElapsed = 0.0f;

    [Header("UI")]
    [SerializeField]
    GameObject UIWidgetWorldAnchor = null;
    [SerializeField]
    POISearchLostObject UIWidgetLostObjectPrefab = null;

    private POISearchLostObject UIWidgetLostObject = null;

    void Awake()
    {
        Agent = GetComponent<NavMeshAgent>();
        AnimatorComponent = GetComponent<Animator>();
    }

    void Start()
    {
        //AnimatorComponent.Play("Walk");
    }

    void Update()
    {
        UpdateState(CurrentState);
    }

    void SetState(State _state)
    {
        if (_state != CurrentState)
        {
            ExitState(CurrentState);
        }
        EnterState(_state);
        m_state = _state;
    }
    private AIPlanningManager.SearchPositionHandler SelectSearchPositionFromManager()
    {
        return AIPlanningManager.Instance.SelectRandomSearchPosition();
    }
    public void AssignCharacter(AIPlanningManager.CharacterHandler _character)
    {
        AssignedCharacter = _character;
        AssignedCharacter.Occupied = true;
    }
    public void AssignTable(AIPlanningManager.TableHandler _table)
    {
        AssignedTable = _table;
        AssignedTable.Occupied = true;
        SetState(State.MoveToAssignedTable);
    }

    public void AssignLostObject(AIPlanningManager.LostObjectHandler _lostObject)
    {
        AssignedLostObject = _lostObject;
        AssignedLostObject.Occupied = true;
    }
    public bool CanGiveLostObject()
    {
        return CurrentState == State.Search;
    }
    public bool IsLostObject(GameObject _gameObject)
    {
        if (AssignedLostObject == null)
            return false;

        return _gameObject.name.StartsWith(AssignedLostObject.LostObjectData.label);
    }
    public void GiveLostObject(GameObject _gameObject)
    {
        HasFoundLostObject = true;
    }
    public void NotifyGiveObjectStarted()
    {
        GiveObjectStarted = true;
    }

    void EnterState(State _state)
    {
        switch(_state)
        {
            case State.MoveToAssignedTable:
                Vector3 tablePos = AssignedTable.Table.EnterTableTRS.position;
                Agent.destination = tablePos;
                AnimatorComponent.Play("Walk");
                break;
            case State.Eat:
                TimeEatElapsed = 0.0f;
                AnimatorComponent.Play("Eat");
                Agent.enabled = false;
                transform.position = AssignedTable.Table.EatTRS.position;
                transform.rotation = AssignedTable.Table.EatTRS.rotation;
                DisplayCocaCup(true);
                break;
            case State.MoveToSearchPos:
                if (AssignedTable != null)
                {
                    AssignedTable.Occupied = false;
                    AssignedTable = null;
                }
                break;
            case State.Search:
                AnimatorComponent.Play("Search");
                Agent.enabled = false;

                GameObject uiWidgetParent = GameObject.Find("POIs");
                if (uiWidgetParent != null)
                {
                    UIWidgetLostObject = Instantiate<POISearchLostObject>(UIWidgetLostObjectPrefab, uiWidgetParent.transform);
                    UIWidgetLostObject.ItemData = AssignedLostObject.LostObjectData;
                    UIWidgetLostObject.Holder = UIWidgetWorldAnchor;
                    UIWidgetLostObject.timeToFindItem = SearchDuration;
                }
                break;
            case State.Happy:
                AnimatorComponent.Play("Happy");
                break;
            case State.LeaveScene:
                if (AssignedSearchPos != null)
                {
                    AssignedSearchPos.Occupied = false;
                    AssignedSearchPos = null;
                }

                Agent.destination = AIPlanningManager.Instance.GetExitPosition();
                if (IsSad)
                {
                    AnimatorComponent.Play("Sad");
                }
                else
                {
                    AnimatorComponent.Play("Walk");
                }
                break;
            default:
                break;
        }
    }
    void ExitState(State _state)
    {

        switch (_state)
        {
            case State.Eat:
                if (AssignedLostObject != null && AssignedTable != null)
                {
                    GameObject gameObject = Instantiate(AssignedLostObject.LostObjectData.prefab, AssignedTable.Table.LostObjectTRS.position, Quaternion.identity);
                    gameObject.name = AssignedLostObject.LostObjectData.label;
                    transform.position = AssignedTable.Table.ExitTableTRS.position;
                    transform.rotation = AssignedTable.Table.ExitTableTRS.rotation;
                    Agent.enabled = true;
                }
                DisplayCocaCup(false);
                break;
            case State.MoveToSearchPos:
                Agent.updateRotation = true;
                Agent.updatePosition = true;
                Agent.isStopped = false;
                break;
            case State.Search:
                Agent.enabled = true;
                if (UIWidgetLostObject != null)
                {
                    Destroy(UIWidgetLostObject.gameObject);
                }
                break;
            default:
                break;
        }
    }
    public void DisplayCocaCup(bool value)
    {
        if (CocaCup != null)
        {
            CocaCup.SetActive(value);
        }
    }
    private bool IsPathInProgress()
    {
        return Agent.pathPending;
    }
    void UpdateState(State _state)
    {
        switch (_state)
        {
            case State.MoveToAssignedTable:
                if (IsPathInProgress())
                    break;

                if (Agent.remainingDistance <= DestinationEpsilon)
                {
                    SetState(State.Eat);
                }
                break;
            case State.Eat:
                TimeEatElapsed += Time.deltaTime; 
                if (TimeEatElapsed >= EatDuration)
                {
                    SetState(State.MoveToSearchPos);
                }
                break;
            case State.MoveToSearchPos:

                if (AssignedSearchPos == null)
                {
                    AssignedSearchPos = SelectSearchPositionFromManager();
                    if (AssignedSearchPos != null)
                    {
                        AssignedSearchPos.Occupied = true;
                        Agent.destination = AssignedSearchPos.TRS.position;
                    }
                }

                if (AssignedSearchPos == null || IsPathInProgress())
                    break;

                AnimatorComponent.Play("Walk");

                if (Agent.remainingDistance <= DestinationEpsilon)
                {
                    Agent.isStopped = true;
                    Agent.updateRotation = false;
                    Agent.velocity = new Vector3(0.0f, 0.0f, 0.0f);
                    Agent.destination = transform.position;
                    
                    Quaternion targetRotation = AssignedSearchPos.TRS.rotation;
                    //transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, 1.0f * Time.deltaTime);
                    transform.rotation = targetRotation;
                    float angleDiff = Mathf.Abs(transform.rotation.eulerAngles.y - targetRotation.eulerAngles.y);
                    if (angleDiff < 1.5f)
                    {
                        SetState(State.Search);
                    }
                }
                break;
            case State.Search:
                if (HasFoundLostObject)
                {
                    float timeElapsedFactor = TimeSearchElapsed / SearchDuration;
                    ScoreManager.instance.GainPoints(1.0f - timeElapsedFactor);
                    SetState(State.Happy);
                    break;
                }

                TimeSearchElapsed = Mathf.Clamp(TimeSearchElapsed + Time.deltaTime, 0.0f, SearchDuration);
                if (TimeSearchElapsed >= SearchDuration && !GiveObjectStarted)
                {
                    IsSad = true;
                    ScoreManager.instance.LosePoints();
                    SetState(State.LeaveScene);
                }
                else if (TimeSearchElapsed >= Mathf.Abs(SearchDuration - AngryDuration))
                {
                    AnimatorComponent.Play("Angry");
                }
                break;
            case State.Happy:
                TimeHappyElapsed += Time.deltaTime;
                if (TimeHappyElapsed >= HappyDuration)
                {
                    SetState(State.LeaveScene);
                }
                break;
            case State.LeaveScene:
                if (IsPathInProgress())
                    break;

                if (Agent.remainingDistance <= DestinationEpsilon)
                {
                    if (AssignedCharacter != null)
                    {
                        AssignedCharacter.Occupied = false;
                        AssignedCharacter = null;
                    }
                    if (AssignedLostObject != null)
                    {
                        if (IsSad)
                        {
                            AIPlanningManager.Instance.FlagLostObjectAsLostForever(AssignedLostObject);
                        }
                        else
                        {
                            AssignedLostObject.Occupied = false;
                        }
                        AssignedLostObject = null;
                    }
                    Destroy(this.gameObject);
                }
                break;
            default:
                break;
        }
    }
}
