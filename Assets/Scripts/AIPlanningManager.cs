﻿using System.Collections.Generic;
using UnityEngine;


public class AIPlanningManager : MonoBehaviour
{

    public class TableHandler
    {
        public TableHandler(Table _table)
        {
            Table = _table;
        }

        public Table Table = null;
        public bool Occupied = false;
    }
    public class SearchPositionHandler
    {
        public SearchPositionHandler(Transform _trs)
        {
            TRS = _trs;
        }

        public Transform TRS = null;
        public bool Occupied = false;
    }
    [System.Serializable]
    public class LostObjectHandler
    {
        public LostObjectHandler(LostItemData _lostItemData)
        {
            LostObjectData = _lostItemData;
        }

        public LostItemData LostObjectData;
        public bool Occupied = false;
    }

    public class CharacterHandler
    {
        public CharacterHandler(AICharacter prefab)
        {
            Prefab = prefab;
        }

        public AICharacter Prefab = null;
        public bool Occupied = false;
    }

    [SerializeField]
    int CharacterSpawnCountAtStart = 0;

    [SerializeField]
    int LostObjectCountAtStart = 0;

    [SerializeField]
    private List<Table> Tables = new List<Table>();
    private List<TableHandler> TableHandlers = new List<TableHandler>();

    [SerializeField]
    private List<LostItemData> LostObjects = new List<LostItemData>();
    private List<LostObjectHandler> LostObjectHandlers = new List<LostObjectHandler>();

    private List<LostObjectHandler> LostObjectLostForever = new List<LostObjectHandler>();
    private List<string> LostObjectDroppedInLostAndFoundBox = new List<string>();

    [SerializeField]
    private List<AICharacter> CharacterPrefabs = new List<AICharacter>();
    private List<CharacterHandler> CharacterHandlers = new List<CharacterHandler>();

    [SerializeField]
    private List<Transform> SearchPositions = new List<Transform>();
    private List<SearchPositionHandler> SearchPositionHandlers = new List<SearchPositionHandler>();

    [SerializeField]
    Transform SpawnPointTRS = null;
    [SerializeField]
    float SpawnPointRadius = 1.0f;

    [SerializeField]
    Vector2 SpawnDelay = new Vector2(5.0f, 20.0f);
    [SerializeField]
    float TimeBeforeNextSpawn = 2.0f;

    static public AIPlanningManager Instance = null;
    private void Awake()
    {
        if (Instance != null)
        {
            Debug.LogError("Only one instance of AIPlanningManager is permitted.");
            DestroyImmediate(this);
            return;
        }

        Instance = this;

        for (int i = 0; i < Tables.Count; ++i)
        {
            Table table = Tables[i];
            if (table == null)
            {
                Debug.LogError("table with index " + i + " cannot be null");
                continue;
            }
            TableHandlers.Add(new TableHandler(Tables[i]));
        }

        for (int i = 0; i < SearchPositions.Count; ++i)
        {
            SearchPositionHandlers.Add(new SearchPositionHandler(SearchPositions[i]));
        }

        for (int i = 0; i < LostObjects.Count; ++i)
        {
            LostObjectHandlers.Add(new LostObjectHandler(LostObjects[i]));
        }

        for (int i = 0; i < CharacterPrefabs.Count; ++i)
        {
            CharacterHandlers.Add(new CharacterHandler(CharacterPrefabs[i]));
        }
    }
    private void Start()
    {
        for (int i = 0; i < LostObjectCountAtStart; ++i)
        {
            Vector3 position = SelectRandomAvailableTable().Table.LostObjectTRS.position;
            Instantiate(SelectRandomAvailableLostObject().LostObjectData.prefab, position, Quaternion.identity);
        }

        for (int i = 0; i < CharacterSpawnCountAtStart; ++i)
        {
            if (!TrySpawnCharacter())
                break;
        }
    }
    private void Update()
    {
        TimeBeforeNextSpawn -= Time.deltaTime;
        if (TimeBeforeNextSpawn <= 0.0f)
        {
            TimeBeforeNextSpawn += GetRandomSpawnDelay();
            TrySpawnCharacter();
        }
    }
    bool TrySpawnCharacter()
    {
        CharacterHandler characterHandler = SelectRandomAvailableCharacter();
        if (characterHandler == null)
            return false;
        TableHandler tableHandler = SelectRandomAvailableTable();
        if (tableHandler == null)
            return false;
        LostObjectHandler lostObjectHandler = SelectRandomAvailableLostObject();
        if (lostObjectHandler == null)
            return false;

        AICharacter character = Instantiate(characterHandler.Prefab, GetRandomSpawnPosition(), SpawnPointTRS.rotation);
        character.AssignCharacter(characterHandler);
        character.AssignLostObject(lostObjectHandler);
        character.AssignTable(tableHandler);

        return true;
    }
    float GetRandomSpawnDelay()
    {
        return Random.Range(SpawnDelay.x, SpawnDelay.y);
    }
    int GetRandomCharacterIndex()
    {
        return Random.Range(0, CharacterHandlers.Count);
    }
    int GetRandomTableIndex()
    {
        return Random.Range(0, TableHandlers.Count);
    }

    int GetRandomLostObjectIndex()
    {
        return Random.Range(0, LostObjectHandlers.Count);
    }
    int GetRandomSearchPositionIndex()
    {
        return Random.Range(0, SearchPositionHandlers.Count);
    }
    public SearchPositionHandler SelectRandomSearchPosition()
    {
        if (SearchPositionHandlers.Count <= 0)
        {
            Debug.LogError("No search position found");
            return null;
        }

        bool searchPositionAvailable = false;
        for (int i = 0; i < SearchPositionHandlers.Count; ++i)
        {
            if (!SearchPositionHandlers[i].Occupied)
            {
                searchPositionAvailable = true;
                break;
            }
        }

        if (!searchPositionAvailable)
            return null;

        SearchPositionHandler selectedSearchPositionHandler;
        do
        { 
            int index = GetRandomSearchPositionIndex();
            selectedSearchPositionHandler = SearchPositionHandlers[index];
        } 
        while (selectedSearchPositionHandler.Occupied);

        return selectedSearchPositionHandler;
    }
    TableHandler SelectRandomAvailableTable()
    {
        if (TableHandlers.Count <= 0)
        {
            Debug.LogError("No table found");
            return null;
        }

        bool tableAvailable = false;
        for (int i = 0; i < TableHandlers.Count; ++i)
        {
            if (!TableHandlers[i].Occupied)
            {
                tableAvailable = true;
                break;
            }
        }

        if (!tableAvailable)
            return null;

        TableHandler selectedTableHandler;
        do
        {
            int index = GetRandomTableIndex();
            selectedTableHandler = TableHandlers[index];
        } 
        while (selectedTableHandler.Occupied);

        return selectedTableHandler;
    }

    CharacterHandler SelectRandomAvailableCharacter()
    {
        if (CharacterHandlers.Count <= 0)
        {
            Debug.LogError("No character found");
            return null;
        }

        return CharacterHandlers[GetRandomCharacterIndex()];
    }
    LostObjectHandler SelectRandomAvailableLostObject()
    {
        if (LostObjectHandlers.Count <= 0)
        {
            Debug.LogError("No LostObject found");
            return null;
        }

        bool lostObjectAvailable = false;
        for (int i = 0; i < LostObjectHandlers.Count; ++i)
        {
            if (!LostObjectHandlers[i].Occupied)
            {
                lostObjectAvailable = true;
                break;
            }
        }

        if (!lostObjectAvailable)
            return null;

        LostObjectHandler selectedLostObjectHandler;
        do
        {
            int index = GetRandomLostObjectIndex();
            selectedLostObjectHandler = LostObjectHandlers[index];
        }
        while (selectedLostObjectHandler.Occupied);

        return selectedLostObjectHandler;
    }
    public Vector3 GetExitPosition()
    {
        return SpawnPointTRS.position;
    }
    Vector3 GetRandomSpawnPosition()
    {
        Vector2 offsetPos = Random.insideUnitCircle * SpawnPointRadius;
        Vector3 spawnPosition = SpawnPointTRS.position + new Vector3(offsetPos.x, offsetPos.y, 0.0f);
        return spawnPosition;
    }
    public bool MustDropObjectInLostAndFoundBox(GameObject _gameObject)
    {
        for (int i = 0; i < LostObjectLostForever.Count; i++)
        {
            if (_gameObject.name.StartsWith(LostObjectLostForever[i].LostObjectData.label))
                return true;
        }

        return false;
    }
    public void OnGameObjectDroppedInLostAndFoundBox(GameObject _gameObject)
    {
        for (int i = 0; i < LostObjectLostForever.Count; i++)
        {
            if (_gameObject.name.StartsWith(LostObjectLostForever[i].LostObjectData.label))
            {
                LostObjectLostForever[i].Occupied = false;
                LostObjectLostForever.RemoveAt(i);
                return;
            }
        }

        LostObjectDroppedInLostAndFoundBox.Add(_gameObject.name);
    }

    public void FlagLostObjectAsLostForever(LostObjectHandler _lostObject)
    {
        for (int i = 0; i < LostObjectDroppedInLostAndFoundBox.Count; i++)
        {
            if (LostObjectDroppedInLostAndFoundBox[i].StartsWith(_lostObject.LostObjectData.label))
            {
                // the object is already in the LostAndFound box, so we can release the object
                _lostObject.Occupied = false;
                LostObjectDroppedInLostAndFoundBox.RemoveAt(i);
                return;
            }
        }

        LostObjectLostForever.Add(_lostObject);
    }
}
