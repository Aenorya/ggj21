﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using UnityEngine;
using UnityEngine.UI;

public class ObjectsPicker : MonoBehaviour
{
    public float detectionLimit = 10f;
    public LayerMask objectDetectionMask;
    public LayerMask aiDetectionMask;
    public Transform pickerTransform;
    public GameObject pickable;
    public GameObject AI;
    public GameObject lockAI;
    public GameObject dropBox;
    public bool canGiveDropBox;
    public bool lockDropBox = false;

    public bool isGivingObject = false;
    
    public Image screenCenter;
    public Color normalColor, highlightedColor;
    [SerializeField] private bool holdsObject = false;

    public static ObjectsPicker Instance = null;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    private void Start()
    {
        InputManager.controls.Player.Pick.performed += context => OnClick();
    }

    private void Update()
    {
        if (!isGivingObject)
        {
            RaycastHit hit;
            Debug.DrawRay(Camera.main.transform.position, Camera.main.transform.forward * 10f, Color.red);
            if (!holdsObject)
            {
                if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit,
                    detectionLimit,
                    objectDetectionMask))
                {
                    if (hit.transform.childCount > 0)
                    {
                        pickable = hit.transform.gameObject;
                        HighlightPickable(true);
                    }
                }
                else if (pickable)
                {
                    HighlightPickable(false);
                    pickable = null;
                }
            }
            else if (pickable)
            {
                if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit,
                    detectionLimit,
                    aiDetectionMask))
                {
                    if (hit.transform.GetComponent<AICharacter>())
                    {
                        AI = hit.transform.gameObject;
                    }
                }
                else if (AI)
                {
                    AI = null;
                }
            }
        }
    }

    private void HighlightPickable(bool highlight)
    {
        pickable.transform.GetChild(0).gameObject.SetActive(highlight);
        if (screenCenter != null)
        {
            screenCenter.color = highlight ? highlightedColor : normalColor;
        }
    }

    private void StartPicking()
    {
        GetComponent<Animator>().SetTrigger("Pickup");
        holdsObject = true;
        HighlightPickable(false);
    }

    public void PickObject()
    {
        if (pickable)
        {
            pickable.transform.SetParent(pickerTransform);
            pickable.transform.localPosition = Vector3.zero;
            pickable.transform.localScale /= 2f;
        }
    }

    private void OnClick()
    {
        Debug.Log("Click click click");
        if (pickable && !holdsObject)
        {
            StartPicking();
        } else if (pickable && holdsObject && AI)
        {
            AICharacter characterComponent = AI.GetComponent<AICharacter>();
            if (characterComponent != null
                && characterComponent.CanGiveLostObject()
                && characterComponent.IsLostObject(pickable.gameObject))
            {
                characterComponent.NotifyGiveObjectStarted();
                TryGivingBack(false);
            }
        } else if (pickable && holdsObject)
        {
            TryGivingBack(canGiveDropBox);

        }
        
    }
    
    public void TryGivingBack(bool db = false)
    {
        if(!isGivingObject){
            canGiveDropBox = db;
            lockDropBox = canGiveDropBox;
            if (holdsObject && (AI || canGiveDropBox))
            {
                isGivingObject = true;
                if(AI) lockAI = AI;
                GetComponent<Animator>().SetTrigger("GiveBack");
            }
        }
    }
    
    private void LetGoPickable(){
        if (!lockDropBox && lockAI)
        {
            lockAI.GetComponent<AICharacter>().GiveLostObject(pickable.gameObject);
            Destroy(pickable.gameObject);
        }
        else if (lockDropBox && !lockAI)
        {
            pickable.transform.parent = dropBox.transform;
            pickable.transform.localPosition = Vector3.zero;
            pickable.transform.localScale *= 2;
            Rigidbody rigidb = pickable.AddComponent<Rigidbody>();
            rigidb.useGravity = true;
            pickable.layer = 0;

            AIPlanningManager.Instance.OnGameObjectDroppedInLostAndFoundBox(pickable.gameObject);
        }

        lockDropBox = canGiveDropBox;
        isGivingObject = false;
        holdsObject = false;
    }


}
