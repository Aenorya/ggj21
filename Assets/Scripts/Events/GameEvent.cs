﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Custom/Game Event")]
public class GameEvent : ScriptableObject
{
    public string eventName;
    private List<IGameEventListener> listeners = new List<IGameEventListener>();

    public void Subscribe(IGameEventListener listener)
    {
        listeners.Add(listener);
    }

    public void Unsubscribe(IGameEventListener listener)
    {
        listeners.Remove(listener);
    }
    
    public void Notify()
    {
        foreach (IGameEventListener l in listeners)
        {
            l.OnNotify(eventName);
        }
    }
}
