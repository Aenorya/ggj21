﻿using UnityEngine;
using UnityEngine.UI;

public class POIDropObjectInBox : MonoBehaviour
{
    [SerializeField]
    private GameObject Holder = null;
    [SerializeField]
    private Text Text;
    [SerializeField]
    private string Message = "Put object here";

    private RectTransform Rect = null;

    private void Awake()
    {
        Rect = GetComponent<RectTransform>();
    }

    private void Start()
    {
        
    }

    private void Update()
    {
        bool mustDisplayMessage = false;
        if (ObjectsPicker.Instance.pickable != null)
        {
            if (AIPlanningManager.Instance.MustDropObjectInLostAndFoundBox(ObjectsPicker.Instance.pickable))
            {
                Text.text = Message;
                mustDisplayMessage = true;
            }
        }

        if (!mustDisplayMessage)
        {
            Text.text = "";
        }

        if (Holder != null && Camera.main != null)
        {
            Vector3 positionOnScreen = Camera.main.WorldToScreenPoint(Holder.transform.position);
            if (positionOnScreen.z >= 0.0f)
            {
                Rect.anchoredPosition = positionOnScreen;
            }
        }
    }
}
