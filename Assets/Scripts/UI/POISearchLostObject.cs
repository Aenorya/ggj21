﻿using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class POISearchLostObject : MonoBehaviour
{
    [SerializeField]
    public Image ObjectImage = null;
    public Image FillImage;

    [SerializeField]
    public GameObject Holder = null;

    [SerializeField]
    public LostItemData ItemData = null;

    private RectTransform Rect = null;

    [SerializeField] public float timeToFindItem = 100f;
    
    [SerializeField] private Gradient progressionGradient = null;
    
    private float startTime = 0, elapsedTime = 0;
    private bool done = false;

    private void Awake()
    {
        Rect = GetComponent<RectTransform>();
        startTime = Time.time;
        elapsedTime = 0;
    }
    private void Start()
    {
        if (ObjectImage == null)
        {
            Debug.LogError("Object image is null");
            return;
        }

        if (ItemData != null)
        {
            ObjectImage.sprite = ItemData.icon;
        }
    }
    private void Update()
    {

        if (!done)
        {
            elapsedTime = Time.time - startTime;
            if (elapsedTime < timeToFindItem)
            {
                FillImage.fillAmount = (float) elapsedTime / (float) timeToFindItem;
                FillImage.color = progressionGradient.Evaluate(FillImage.fillAmount);
            }
            else
            {
                done = true;
                Debug.Log("CEST FINIIIIIIII !");
            }

            if (Holder != null && Camera.main != null)
            {
                Vector3 positionOnScreen = Camera.main.WorldToScreenPoint(Holder.transform.position);
                if (positionOnScreen.z >= 0.0f)
                {
                    Rect.anchoredPosition = positionOnScreen;
                }
            }
        }
    }

    public float StopTimer()
    {
        done = true;
        return timeToFindItem - elapsedTime;
    }
}
