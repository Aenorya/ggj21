﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public Button Play;

    public Button Quit;
    
    
    // Start is called before the first frame update
    void Start()
    {
        Play.onClick.AddListener(delegate
        {
            Debug.Log("Open scene ");
            SceneManager.LoadScene(1); });
        Quit.onClick.AddListener(delegate { Application.Quit(); });
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
