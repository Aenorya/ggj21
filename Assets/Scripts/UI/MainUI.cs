﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MainUI : MonoBehaviour, IGameEventListener
{
    public List<GameEvent> eventsToListen;
    public TextMeshProUGUI scoreText;
    public Image progress;

    public GameObject winPanel, losePanel;

    public void Start()
    {
        foreach (GameEvent e in eventsToListen)
        {
            e.Subscribe(this);
        }
        UpdateUI();
    }

    public void OnNotify(string eventID)
    {
        switch (eventID)
        {
            case "score":
            {
                UpdateUI();
                break;
            }
            case "win":
            {
                OnWin();
                break;
            }
            case "loss":
            {
                OnLoss();
                break;
            }
        }
    }

    private void UpdateUI()
    {
        Debug.Log("New Score : " + ScoreManager.instance.scorePoints);
        
        scoreText.text = ScoreManager.instance.scorePoints.ToString();
        progress.fillAmount = (float) ScoreManager.instance.scorePoints / (float) ScoreManager.instance.scoreObjective;
    }

    private void OnWin()
    {
        Debug.Log("You won");
        Time.timeScale = 0;
        winPanel.SetActive(true);
    }

    private void OnLoss()
    {
        Time.timeScale = 0;
        losePanel.SetActive(true);
        Debug.Log("You lost");
    }
}
