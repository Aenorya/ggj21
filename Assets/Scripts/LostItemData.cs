﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Custom/Lost Item Data")]
public class LostItemData : ScriptableObject
{
    public string label;
    public Sprite icon;
    public GameObject prefab;
}
