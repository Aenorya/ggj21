﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour
{
    public Controls controls;
    public float speed = 5f;
    public Rigidbody rb;
    public float rotationSpeed = 5f;
    public float cameraRotationSpeed = 5f;
    public Vector2 cameraXRotationLimits;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        controls = InputManager.controls;
    }

    private void FixedUpdate()
    {
        Look(controls.Player.Look.ReadValue<Vector2>());
        Move(controls.Player.Movement.ReadValue<Vector2>());
    }

    private void Move(Vector2 mv)
    {
        if (mv == Vector2.zero)
        {
            rb.velocity = Vector3.zero;
        }
        rb.MovePosition(transform.position + transform.TransformDirection(new Vector3(mv.x, 0, mv.y) * (speed * Time.deltaTime)));
    }

    public void Look(Vector2 lookDirection)
    {
        Quaternion latRot = Quaternion.Euler(Vector3.up * (Time.deltaTime * lookDirection.x * rotationSpeed));
        rb.MoveRotation(rb.rotation * latRot);
        float targetRot =Time.deltaTime * lookDirection.y * cameraRotationSpeed;
        if (targetRot != 0)
        {
            Camera.main.transform.Rotate(-targetRot, 0,0);
        }
    }
}
