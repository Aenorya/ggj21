﻿using UnityEngine;

public class Table : MonoBehaviour
{
    [SerializeField]
    public Transform EnterTableTRS;

    [SerializeField]
    public Transform ExitTableTRS;

    [SerializeField]
    public Transform LostObjectTRS;

    [SerializeField]
    public Transform EatTRS;
    private void Awake()
    {
        if (EnterTableTRS == null)
        {
            Debug.LogError("Table " + gameObject.name + " has invalid enter table trs");
        }

        if (ExitTableTRS == null)
        {
            Debug.LogError("Table " + gameObject.name + " has invalid exit table trs");
        }

        if (LostObjectTRS == null)
        {
            Debug.LogError("Table " + gameObject.name + " has invalid lost object trs");
        }

        if (EatTRS == null)
        {
            Debug.LogError("Table " + gameObject.name + " has invalid eat trs");
        }
    }
}
