﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleDoor : MonoBehaviour
{

    [SerializeField]
    public GameObject LeftDoor = null;
    [SerializeField]
    public GameObject RightDoor = null;

    // Start is called before the first frame update
    void Start()
    {
        OpenDoor();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OpenDoor()
    {
        LeftDoor.transform.Rotate(Vector3.up, 60.0f);
    }
    public void CloseDoor()
    {

    }
}
